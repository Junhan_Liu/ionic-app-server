DROP TABLE IF EXISTS users;

CREATE TABLE users
(
  id        INT AUTO_INCREMENT
    PRIMARY KEY,
  email     VARCHAR(255) NOT NULL UNIQUE,
  password  TEXT         NOT NULL
);

INSERT INTO users (email, password)
VALUES ('test@ionic.com', '123456');
