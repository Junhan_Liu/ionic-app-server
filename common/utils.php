<?php
if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Methods: POST, GET, DELETE, PUT, PATCH, OPTIONS');
    header('Access-Control-Allow-Headers: token, Content-Type');
    header('Access-Control-Max-Age: 1728000');
    header('Content-Length: 0');
    header('Content-Type: text/plain');
    die();
}
//return data
function response_data ($data) {
    return json_encode(array(
        'data' => $data,
        'current_user' => $_SESSION['email']
    ));
}
//return message
function response_message ($message) {
    return json_encode(array(
        'message' => $message,
        'current_user' => $_SESSION['email']
    ));
}
//get data
function get_post_data () {
    return json_decode(file_get_contents('php://input'), true);
}
//check login
function is_login () {
    return !empty($_SESSION['email']);
}
//get current user
function current_user () {
    return $_SESSION['email'];
}